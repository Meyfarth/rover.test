<?php

namespace App\Interfaces;


interface iRover
{

    function __construct(iPlateau $plateau, iPosition $position, iHeading $heading);

    public function getPosition(): iPosition;

    public function getHeading(): iHeading;

    function action(iInstruction $Instruction) ;

}
