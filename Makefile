.PHONY: help
help:
	@echo "Usage:"
	@echo "     make [command]"
	@echo
	@echo "Available commands:"
	@grep -h '^[^#[:space:]].*:' Makefile  | grep -v '^default' | grep -v '^\.' | grep -v '=' | grep -v '^_' | sed 's/://' | xargs -n 1 echo ' -' | sort | uniq
	@echo

init:
	echo "APP_ENV=dev" > .env
	echo "APP_SECRET=3df5905e9a5f8c6812bb7213a8c1fcdb">> .env

install:
	make init
	composer install
	make test

install-env-test:
	make init
	make env-clean
	make env-test 
	make install

run:
	bin/console app:run tests/robots.txt

test: clean-tests
	php -d display_errors=-1 ./vendor/bin/phpunit --bootstrap vendor/autoload.php  --coverage-text --verbose tests/

clean-tests:
	rm -rf ./var/*
	rm -rf ./_reports

